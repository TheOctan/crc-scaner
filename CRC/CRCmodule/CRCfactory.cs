﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRC.CRCmodule
{
	public class CRCfactory
	{
		private uint[] crc_table = new uint[256];
		
		public CRCfactory()
		{
			BuildTable();
		}
		private void BuildTable()
		{
			uint crc;

			for (uint i = 0; i < 256; i++)
			{
				crc = i;
				for (int j = 0; j < 8; j++)
					crc = ((crc & 1) == 1) ? (crc >> 1) ^ 0xEDB88320 : crc >> 1;

				crc_table[i] = crc;
			}
		}

		private uint Crc32(byte[] array)
		{
			uint result = 0xFFFFFFFF;

			for (int i = 0; i < array.Length; i++)
			{
				byte last_byte = (byte)(result & 0xFF);
				result >>= 8;
				result = result ^ crc_table[last_byte ^ array[i]];
			}

			return result;
		}

		public uint GetFileCrc(string filename)
		{
			var fileInfo = new FileInfo(filename);
			var reader = fileInfo.OpenRead();
			var buffer = new byte[reader.Length];

			reader.Read(buffer, 0, (int)reader.Length);

			return Crc32(buffer);
		}
	}
}
