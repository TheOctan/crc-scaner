﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRC.DB
{
	public class DataBase : IDataBase
	{
		private List<FileCode> crcCode = new List<FileCode>();
		public IReadOnlyList<FileCode> CrcCode => crcCode;

		public bool AddFile(string file, uint code)
		{
			crcCode.Add(new FileCode()
			{
				Name = file,
				Code = code
			});

			return true;
		}

		public bool AddFiles(IEnumerable<FileCode> files)
		{
			crcCode.AddRange(files);

			return true;
		}

		public bool Reset()
		{
			crcCode.Clear();

			return true;
		}
	}
}
