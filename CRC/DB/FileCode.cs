﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRC.DB
{
	public class FileCode
	{
		public string Name { get; set; }
		public uint Code { get; set; }
	}
}
