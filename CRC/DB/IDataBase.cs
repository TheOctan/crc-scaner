﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRC.DB
{
	public interface IDataBase
	{
		IReadOnlyList<FileCode> CrcCode { get; }
		bool AddFile(string file, uint code);
		bool AddFiles(IEnumerable<FileCode> files);
		bool Reset();
	}
}
