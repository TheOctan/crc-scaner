﻿using CRC.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRC.Serializer
{
	public interface ISerializer
	{
		bool Serialize(IDataBase data, string fileName);
		bool Deserialize(IDataBase data, string fileName);
	}
}
