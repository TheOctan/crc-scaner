﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRC.DB;
using Newtonsoft.Json;

namespace CRC.Serializer
{
	public class JsonSerializer : ISerializer
	{
		public bool Deserialize(IDataBase data, string fileName)
		{
			if (!File.Exists(fileName))
				return false;

			string output = File.ReadAllText(fileName);

			data.AddFiles(JsonConvert.DeserializeObject<DataBase>(output).CrcCode);
			
			return true;
		}

		public bool Serialize(IDataBase data, string fileName)
		{
			using (var file = new FileStream(fileName, FileMode.Create, FileAccess.Write))
			{
				using (var writer = new StreamWriter(file))
				{
					writer.Write(JsonConvert.SerializeObject(data));
				}
			}

			return true;
		}
	}
}
