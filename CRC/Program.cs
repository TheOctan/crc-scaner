﻿using CRC.CRCmodule;
using CRC.DB;
using CRC.Serializer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CRC
{
	class Program
	{
		static int mode = -1;

		static void Main(string[] args)
		{
			CRCfactory crc = new CRCfactory();
			DataBase data = new DataBase();
			JsonSerializer json = new JsonSerializer();
			json.Deserialize(data, "data.json");

			string currentPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

			if (data.CrcCode.ToList().Exists(x => x.Name == "programcrc"))
			{
				if (data.CrcCode.Where(x => x.Name == "programcrc").FirstOrDefault()?.Code != getCrcOfFolder(currentPath))
				{
					Console.WriteLine("DATA WAS CHANGED");
					Thread.Sleep(2000);
					Environment.Exit(0);
				}
			}
			else
			{

				data.AddFile("programcrc", getCrcOfFolder(currentPath));
				json.Serialize(data, "data.json");
			}


			Console.Write("1) Update data base\n2) Scan direcory\nInput mode: ");
			mode = int.Parse(Console.ReadLine());

			Console.Write("Input name of direcory: ");
			string directory = Console.ReadLine();

			if (!Directory.Exists(directory))
			{
				Console.WriteLine("Directory {0} not found!", directory);
				Console.ReadLine();
				return;
			}

			var files = new DirectoryInfo(directory).GetFiles("*", SearchOption.AllDirectories);

			switch (mode)
			{
				case 1: // Update data base

					// data.Reset();
					foreach (var file in files)
					{
						data.AddFile(file.FullName, crc.GetFileCrc(file.FullName));
					}

					if (json.Serialize(data, "data.json"))
					{
						Console.WriteLine("Data base is serialized");
					}
					else
					{
						Console.WriteLine("Data base is not serialized");
					}
					break;

				case 2: // scan directory

					if (json.Deserialize(data, "data.json"))
					{
						Console.WriteLine("Data base is deserialized");
					}
					else
					{
						Console.WriteLine("Data base is not deserialized");
					}

					bool isModified = false;
					List<string> modifiedFiles = new List<string>();

					foreach (var file in files)
					{
						if (crc.GetFileCrc(file.FullName) != data.CrcCode.Where(x => x.Name == file.FullName).FirstOrDefault()?.Code)
						{
							isModified = true;
							modifiedFiles.Add(file.FullName);
						}
					}

					if (isModified)
					{
						Console.WriteLine("Modified Files: ");
						foreach (var item in modifiedFiles)
						{
							Console.WriteLine(item);
						}
					}
					else
					{
						Console.WriteLine("No Modified Files");
					}

					json.Serialize(data, "data.json");
					break;

				default:
					break;
			}
			Console.ReadKey();
		}

		public static uint getCrcOfFolder(string folderPath)
		{
			var files = new DirectoryInfo(folderPath).GetFiles("*", SearchOption.AllDirectories);
			uint crcCount = 0;

			CRCfactory crc = new CRCfactory();

			foreach (var file in files)
			{
				if (file.Name != "data.json")
					crcCount += crc.GetFileCrc(file.FullName);
			}

			return crcCount;
		}
	}
}
